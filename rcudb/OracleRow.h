// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT Oracle interface 
//
/** @file    OracleRow.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Oracle::Row class
*/
#ifndef RCUDB_ORACLE_ROW
#define RCUDB_ORACLE_ROW
#ifndef RCUDB_ROW
# include <rcudb/Row.h>
#endif
#ifndef __OCCI_H__
# include <occi.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

namespace RcuDb 
{
  /** @defgroup Oracle Oracle interface classes 
      @ingroup DB 
  */
  /** @namespace  Oracle 
      @brief Name space for Oracle classes 
      @ingroup Oracle
  */
  namespace Oracle 
  {
    // forward decls 
    class Result;
    
    /** @class Row
	@brief Specialisation of result row for Oracle 
	@ingroup Oracle
     */ 
    class Row : public RcuDb::Row 
    {
    public:
      /** Destructor */
      virtual ~Row() { Close(); }
      /** Close the result */
      virtual void Close();
      /** Get field as a string 
	  @param i field number */
      virtual const char* FieldStr(int i);
      /** Get length of field string 
	  @param i Field number */ 
      virtual unsigned long FieldLen(int i);
    private:
      /** Get the string corresponding to a field */ 
      std::string DecodeField(int i );
      /** The current result set */ 
      oracle::occi::ResultSet* fResult;
      /** Meta data */ 
      typedef std::vector<oracle::occi::MetaData> MetaVector;
      /** Meta data */ 
      MetaVector* fMeta;
      /** Field count */
      int fNFields;
      /** Buffer */
      typedef std::vector<char*> Buffer_t;
      /** Buffer */ 
      Buffer_t fBuffer;
      /** Constructor */ 
      Row(oracle::occi::ResultSet* r, MetaVector* m);
      /** DEfault CTOR  hidden */
      Row();
      /** Copy ctor hidden */ 
      Row(const Row& other);
      /** Declare result to be a friend */ 
      friend class Result;
      /** Check if this is valid */
      bool Validate(int i);
      /** Get row data */ 
      void GetRowData();
    };
  }
}
#endif
//
// EOF
// 
