//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT MySQL interface 
//
/** @file    MysqlServer.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:43:04 2007
    @brief   Definition of Mysql::Server class 
*/
#include "MysqlServer.h"
#include "MysqlResult.h"
#include "Sql.h"
#include <iostream>

//====================================================================
RcuDb::Mysql::Server::Server(const Url& url) 
  : RcuDb::Server(url), 
    fConnection(0)
{
  if (fUrl.Port() <= 0) fUrl.SetPort(MYSQL_PORT);

  fConnection = new MYSQL;
  mysql_init(fConnection);
  
  std::string socket;
  unsigned long flags = 0;
  
  typedef std::vector<std::string> tokens;
  tokens opts = fUrl.Options();
  for (tokens::iterator i = opts.begin(); i != opts.end(); ++i) {
    std::string::size_type eq = i->find('=');
    std::string opt = i->substr(0, eq);
    std::string arg = i->substr(eq+1, i->size()-eq-1);
    if (opt == "timeout") {
      unsigned int to = 0;
      std::stringstream s(arg);
      s >> to;
      mysql_options(fConnection, MYSQL_OPT_CONNECT_TIMEOUT, 
		    reinterpret_cast<const char*>(&to));
    }
    else if (opt == "socket") 
      socket = arg;
    else if (opt == "multi_statements") 
      flags |= CLIENT_MULTI_STATEMENTS;
    else if (opt == "multi_results") 
      flags |= CLIENT_MULTI_RESULTS;
  }
  std::string db = fUrl.Path();
  if (db[0] == '/') db.erase(0,1);
  fUrl.SetPath(db);

  if (!mysql_real_connect(fConnection, 
			  fUrl.Host().c_str(), 
			  fUrl.User().c_str(), 
			  fUrl.Password().c_str(), 
			  fUrl.Path().c_str(), 
			  fUrl.Port(), 
			  (socket.empty() ? 0 : socket.c_str()), 
			  flags)) {
    fUrl.SetPort(-1);
    SetError(mysql_errno(fConnection), mysql_error(fConnection));
  }
}

//____________________________________________________________________
bool
RcuDb::Mysql::Server::CheckConnection() 
{
  ClearError();
  if (IsConnected()) return true;
  
  SetError(-1, "Not connected to MySQL server");
  return false;
}

//____________________________________________________________________
bool
RcuDb::Mysql::Server::CheckError(bool force)
{
  unsigned int err = mysql_errno(fConnection);
  if (err == 0 && !force) return true;
  

  std::string str = mysql_error(fConnection);
  if (err == 0) {
    err = 11111;
    str = "MySQL error";
  }
  SetError(err, str);
  return false;
}


//____________________________________________________________________
RcuDb::Mysql::Server::~Server()
{
  if (!IsConnected()) return;
  Close();
  delete fConnection;
}


//____________________________________________________________________
void
RcuDb::Mysql::Server::Close()
{
  ClearError();
  if (!fConnection) return;
  mysql_close(fConnection);
  fUrl.SetPort(-1);
}


//____________________________________________________________________
RcuDb::Result* 
RcuDb::Mysql::Server::Query(const Sql& query) 
{
  if (!CheckConnection()) return 0;

  const std::string& q = query.Text();
  if (mysql_real_query(fConnection, q.c_str(), q.size())) { 
    CheckError(true);
    return 0;
  }
  
  MYSQL_RES* res = mysql_store_result(fConnection);
  if (!CheckError(false)) return 0;
  
  return new Result(res);
}


//____________________________________________________________________
bool
RcuDb::Mysql::Server::Exec(const Sql& query) 
{
  if (!CheckConnection()) return false;
  
  const std::string& q = query.Text();
  if (mysql_real_query(fConnection, q.c_str(), q.size())) 
    return CheckError(true);
  return !IsError();
}

//____________________________________________________________________
bool 
RcuDb::Mysql::Server::StartTransaction() 
{
  if (!CheckConnection()) return false;
  return Exec("START TRANSACTION");
}


//____________________________________________________________________
bool 
RcuDb::Mysql::Server::Commit()
{
  if (!CheckConnection()) return false;
  if (mysql_commit(fConnection)) return CheckError(true);
  return true;
}


//____________________________________________________________________
bool 
RcuDb::Mysql::Server::RollBack()
{
  if (!CheckConnection()) return false;
  if (mysql_rollback(fConnection)) return CheckError(true);
  return true;
}


//
// EOF
//
