// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT DB interface 
//
/** @file    Result.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:24:26 2007
    @brief   Declaration of Result class 
*/
#ifndef RCUDB_RESULT
#define RCUDB_RESULT

namespace RcuDb
{
  // Forward declaration 
  class Row;
  
  /** @class Result Result.h <rcudb/Result.h>
      @ingroup DB
      @brief Result set. */
  class Result 
  {
  public:
    /** Destructor */
    virtual ~Result() {}
    /** Close the result */
    virtual void Close() = 0;
    /** Get the number of fields in this result set */ 
    virtual int NFields() = 0;
    /** Get the name of the field @a i */
    virtual const char* FieldName(int i) = 0;
    /** Get the number of rows in this result */ 
    virtual int NRows() const { return fNRows; }
    /** Get the next row in the result set.  This can be used to
	iterate over the result set. 
	@return next row, or 0 if there's no more rows left */
    virtual Row* Next() = 0;
    /** Utility function to print result of query */
    virtual void Print();
  protected:
    /** Constructor is hidden */
    Result() : fNRows(0) {}
    /** Number of rows in the result set. */
    int fNRows;
  };  
}
#endif
//
// EOF
//
