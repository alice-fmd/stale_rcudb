// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT Oracle interface 
//
/** @file    OracleServer.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Oracle::Server class
*/
#ifndef RCUDB_ORACLE_SERVER
#define RCUDB_ORACLE_SERVER
#ifndef RCUDB_SERVER
# include <rcudb/Server.h>
#endif
#ifndef __OCCI_H__
# include <occi.h>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace RcuDb 
{
  // Forward decls 
  class Result;
  
  namespace Oracle 
  {
    // forward decls 
    /** @class Server 
	@brief Specialisation of server server for Oracle 
	@ingroup Oracle
    */ 
    class Server : public RcuDb::Server 
    {
    public:
      /** Perform a query 
	  @param query SQL Query as a string 
	  @return A result set.  The user should manage that memory */ 
      Result* Query(const Sql& query);
      /** Do a query with no result set returned. 
	  @param query SQL Query 
	  @return @c true on success, @c false otherwise */
      bool Exec(const Sql& query);
      /** Start a transaction 
	  @return @c true on succcess */ 
      virtual bool StartTransaction();
      /** End the transaction by commiting changes 
	  @return @c true on succcess */ 
      virtual bool Commit();
      /** Roll back to last started transaction state 
	  @return @c true on succcess */ 
      virtual bool RollBack();
      /** Close the connection */
      virtual void Close();
      /** Destructor */
      virtual ~Server();
    private:
      /** Declare base class to be a friend */ 
      friend class RcuDb::Server;
      /** Constructor 
	  @param url URL of the database connection */ 
      Server(const Url& url);
      /** Check if there's any errors 
	  @param force Force creation of error 
	  @return @c true if there's no error, @c false otherwise */
      bool CheckError(bool force);
      /** Check the connection 
	  @return @c true if we're connected */
      bool CheckConnection();
      /** Environment */
      oracle::occi::Environment* fEnv;
      /** Connection */
      oracle::occi::Connection* fCon;
      /** Information */
      std::string fInfo;
    };
  }
}
#endif
//
// EOF
// 
