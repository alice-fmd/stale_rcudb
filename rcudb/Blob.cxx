//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT MySQL interface 
//
/** @file    Blob.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:12:47 2007
    @brief   Definition of Blob class 
*/
#include "Blob.h"
#include <iostream>

namespace 
{
  const char fgHex[] = { '0', '1', '2', '3', '4', '5',
			 '6', '7', '8', '9', 'A', 'B',
			 'C', 'D', 'E', 'F' };
}

//____________________________________________________________________
void
RcuDb::Blob::Encode(unsigned char* data, size_t n) 
{
  fData.resize(2 * n + 1);
  for (size_t i = 0; i < n; i++) {
    fData[2 * i + 0] = fgHex[size_t((data[i] >> 4) & 0xf)];
    fData[2 * i + 1] = fgHex[size_t((data[i] >> 0) & 0xf)];
    fData[2 * i + 2] = '\0';
  }
}

//____________________________________________________________________
void
RcuDb::Blob::Decode(std::vector<unsigned char>& data) const
{
  data.resize((fData.size()) / 2);
  for (size_t i = 0; i < data.size(); i++) {
    size_t j, k;
    for (j = 0; j < 16; j++) 
      if (fData[2 * i] == fgHex[j]) break;
    for (k = 0; k < 16; k++) 
      if (fData[2 * i + 1] == fgHex[k]) break;
    data[i] = (j << 4) + k;
  }
}

//____________________________________________________________________
std::ostream& 
RcuDb::operator<<(std::ostream& o, const RcuDb::Blob& b)
{
  return o << b.String();
}

//____________________________________________________________________
std::istream& 
RcuDb::operator>>(std::istream& i, RcuDb::Blob& b) 
{
  std::string s;
  i >> s;
  b = s;
  return i;
}

//____________________________________________________________________
//
// EOF
//
