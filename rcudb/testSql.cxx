#include "Sql.h"
#include <iostream>
#include <iomanip>
#include <vector>

int
main(int arc, char** argv) 
{
  RcuDb::Sql sql;
  std::cout << "SQL: " << sql.Text() << std::endl;
  sql << "SELECT * FROM";
  std::cout << "SQL: " << sql.Text() << std::endl;
  sql << " Config WHERE id=" << 10;
  std::cout << "SQL: " << sql.Text() << std::endl;

  std::vector<int> v;
  for (int i = 0; i < 3; i++) v.push_back(i);
  char* blob = (char*)(&v[0]);
  sql << " " << blob;
  std::cout << "SQL: " << sql.Text() << std::endl;
  
  std::string s;
  s.append(blob, sizeof(int) * v.size());
  std::cout << "S: '" << s << "' " << s.size() << " <> " 
	    << (sizeof(int) * v.size()) << std::endl;
  const char* foo = s.c_str();
  int* vv = (int*)(foo);
  for (size_t i = 0; i < s.size() / sizeof(int); i++) 
    std::cout << vv[i] << " <> " << v[i] << std::endl;
  
  return 0;
}

