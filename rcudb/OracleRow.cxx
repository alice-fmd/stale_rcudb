//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT Oracle interface 
//
/** @file    OracleRow.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:03:14 2007
    @brief   Implementation of Oracle::Row class
*/
#include "OracleRow.h"
#ifndef __STRING__
# include <string>
#endif
#include <iostream>

//____________________________________________________________________
RcuDb::Oracle::Row::Row(oracle::occi::ResultSet* r, MetaVector* m)
  : fResult(r), 
    fMeta(m)
{
  using namespace oracle::occi;
  fNFields = m->size();

  if (!fResult || !fMeta || fNFields <= 0) return;
  // char nbuf[256];

  // Some of this may throw.  That's OK, we catch it in the server. 
  std::string res;
  for (size_t i = 1; i <= fNFields; i++) {
    res = DecodeField(i);
    if (res.length() == 0) {
      fBuffer.push_back(0);
      continue;
    }

    char* dat = new char[res.length()+1];
    std::copy(res.begin(), res.end(), dat);
    dat[res.length()] = '\0';
    fBuffer.push_back(dat);
  }
}

std::string
RcuDb::Oracle::Row::DecodeField(int i)
{
  using namespace oracle::occi;
  std::string res("");
  if (fResult->isNull(i)) return res;

  int datatype = (*fMeta)[i-1].getInt(MetaData::ATTR_DATA_TYPE);
  switch (datatype) {
  case SQLT_NUM: { // Number 
    int prec  = (*fMeta)[i-1].getInt(MetaData::ATTR_PRECISION);
    int scale = (*fMeta)[i-1].getInt(MetaData::ATTR_SCALE);
    if (prec == 0 || scale == 0) {
      res = fResult->getString(i);
    }
    else {
      double val = fResult->getDouble(i);
      std::stringstream ds;
      ds << val;
      res = ds.str();
    }
  }
    break;
  case SQLT_CHR: // Character string 
  case SQLT_VCS: // Variable character string 
  case SQLT_AFC: // ANSI fixed character string 
  case SQLT_AVC: // ANSI variable character string 
    res = fResult->getString(i);
    break;
  case SQLT_DAT: 
    res = fResult->getDate(i).toText("MM/DD/YYYY, HH24::MI::SS");
    break;
  case SQLT_TIMESTAMP:
  case SQLT_TIMESTAMP_TZ:
  case SQLT_TIMESTAMP_LTZ:
    res = fResult->getTimestamp(i).toText("MM/DD/YYYY, HH24::MI::SS",0);
  default:
    res = fResult->getString(i); // "";
  } 
  return res;
}
//____________________________________________________________________
void
RcuDb::Oracle::Row::Close() 
{
  for (Buffer_t::iterator i = fBuffer.begin(); i != fBuffer.end(); i++) 
    delete [] (*i);
  fBuffer.clear();
  fMeta    = 0;
  fResult  = 0;
  fNFields = 0;
}

//____________________________________________________________________
const char* 
RcuDb::Oracle::Row::FieldStr(int i) 
{
  if (!Validate(i)) return 0;
  return fBuffer[i];
}

//____________________________________________________________________
unsigned long
RcuDb::Oracle::Row::FieldLen(int i) 
{
  if (!Validate(i)) return 0;

  oracle::occi::MetaData meta = (*fMeta)[i];
  return meta.getInt(oracle::occi::MetaData::ATTR_DATA_SIZE);
}

//____________________________________________________________________
bool
RcuDb::Oracle::Row::Validate(int i)
{
  if (!fResult) return false; // Closed
  if (i < 0 || i >= fNFields) return false;
  return true;
}

//
// EOF
//
