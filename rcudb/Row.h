// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT DB interface 
//
/** @file    Row.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:20:59 2007
    @brief   Declaration of Row class 
*/
#ifndef RCUDB_ROW
#define RCUDB_ROW
#ifndef __SSTREAM__
# include <sstream>
#endif

namespace RcuDb
{
  //================================================================
  /** @class Row Row.h <rcudb/Row.h>
      @ingroup DB
      @brief A row in a result set  */
  class Row 
  {
  public:
    /** Destructor */
    virtual ~Row() {}
    /** Close the result */
    virtual void Close() = 0;
    /** Get field as a string 
	@param i field number */
    virtual const char* FieldStr(int i) = 0;
    /** Get length of field as a string 
	@param i field number */
    virtual unsigned long FieldLen(int i) = 0;
    /** Get field as any type 
	@param i Field number 
	@param r Return value */ 
    template <typename T> 
    bool Field(int i, T& r);
  protected: 
    /** Constructor is hidden */ 
    Row() {}
  };

  //__________________________________________________________________
  template <typename T>
  inline bool 
  Row::Field(int i, T& r) 
  {
    if (FieldLen(i) <= 0) return false;
    std::stringstream s(FieldStr(i));
    s >> r;
    return s.bad() ? false : true;
  }
}

#endif
//
// EOF
//

