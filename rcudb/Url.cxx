//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT MySQL interface 
//
/** @file    Url.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:03:14 2007
    @brief   Implementation of Url class
*/
#include "Url.h"
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cctype>
#include <algorithm>

namespace 
{
  struct to_lower 
  {
    char operator()(char c) const { return std::tolower(c); }
  };
}

     
//____________________________________________________________________
void
RcuDb::Url::Parse(const std::string& url) 
{
  fUrl      = url;
  fScheme   = "";
  fUser     = "";
  fPassword = "";
  fHost     = "";
  fPort     = -1;
  fPath     = "";
  fQuery    = "";
  fAnchor   = "";
  std::string::size_type cur   = 0;
  // Check for protocol 
  std::string::size_type colon = url.find(':');
  if (colon != std::string::npos) {
    fScheme = url.substr(cur, colon);
    cur       = colon + 1;
    // Convert to lower case 
    std::transform(fScheme.begin(),fScheme.end(),fScheme.begin(),to_lower());

    // Check if we have slashes 
    if (url[cur] == '/') cur++;
    if (url[cur] == '/') cur++;
  }
  // Check for an at sign.  If we find one, it indicates login
  // information. 
  std::string::size_type at = url.find('@', cur);
  if (at != std::string::npos) {
    // See if we have a colon in the range [cur,at].  If so, that
    // separates the user name from the password. 
    colon = url.find(':', cur);
    if  (colon != std::string::npos && colon < at) {
      fUser     = url.substr(cur,       colon - cur);
      fPassword = url.substr(colon + 1, at - colon - 1);
    }
    else 
      fUser = url.substr(cur, at - cur);
    cur = at + 1;
  }
  
  // Check for a slash.  If we find one, it indicates the presence
  // of a host name, and optionally a port number 
  std::string::size_type slash = url.find('/', cur);
  if (slash != std::string::npos) {
    // check if we have a port number 
    colon = url.find(':', cur);
    if  (colon != std::string::npos && colon < slash) {
      fHost     = url.substr(cur,       colon - cur);
      std::stringstream s(url.substr(colon + 1, colon - slash - 1));
      s >> fPort;
    }
    else 
      fHost = url.substr(cur, slash - cur);
    // Convert to lower case 
    std::transform(fHost.begin(),fHost.end(),fHost.begin(),to_lower());
    cur = slash;
  }
  
  // Check for a question mark.  If that is found, then we have a
  // query. 
  std::string::size_type question = url.find('?', cur);
  std::string&           next     = (question == std::string::npos 
				     ? fPath : fQuery);
  if (question != std::string::npos) {
    fPath = url.substr(cur, question - cur);
    cur   = question + 1;
  }
  
  // Check for a hash mark.  If that is found, we have a fragment
  std::string::size_type hash = url.find('#', cur);
  std::string&           rem  = (hash != std::string::npos ? fAnchor : next);
  if (hash != std::string::npos) {
    next    = url.substr(cur, hash - cur);
    cur     = hash + 1;
  }
  rem = url.substr(cur);
}

//____________________________________________________________________
/** Utility macro 
 */
#define _URL_LEN(X,Y) \
  (X.empty() ? 0 : std::max(X.size(), strlen(Y))) 


//____________________________________________________________________
const std::vector<std::string> 
RcuDb::Url::Options() const 
{
  std::vector<std::string> ret;
  if (fQuery.empty()) return ret;
  std::istringstream opts(fQuery);
  bool cont = true;
  do {
    std::string s;
    std::getline(opts, s, '&');
    cont = (!opts.eof());
    if (!s.empty()) ret.push_back(s);
  } while (cont);
  return ret;
}

    

//____________________________________________________________________
void
RcuDb::Url::Print() const 
{
  std::cout << "Original: " << fUrl << "\nParsed: " << std::flush;
    
  size_t scheme = _URL_LEN(fScheme,   "scheme");
  size_t user   = _URL_LEN(fUser,     "user");
  size_t passwd = _URL_LEN(fPassword, "passwd");
  size_t host   = _URL_LEN(fHost,     "host");
  size_t port   = (fPort < 0 ? 0 : std::max(size_t(log10(fPort)), 
					    strlen("port")));
  size_t path   = _URL_LEN(fPath,     "path");
  size_t query  = _URL_LEN(fQuery,    "query");
  size_t anchor = _URL_LEN(fAnchor,   "anchor");
    
  if (scheme > 0) std::cout << std::setw(scheme) << fScheme << ":";
  if (user   > 0) {
    std::cout << std::setw(user) << fUser;
    if (passwd > 0) 
      std::cout << ":" << std::setw(passwd) << fPassword;
    std::cout << "@";
  }
  if (host > 0) {
    std::cout << std::setw(host) << fHost;
    if (port > 0) 
      std::cout << ":" << std::setw(port) << fPort;
    // std::cout << "/";
  }
  if (path   > 0) std::cout << std::setw(path) << fPath;
  if (query  > 0) std::cout << '?' << std::setw(query) << fQuery;
  if (anchor > 0) std::cout << '#' << std::setw(anchor) << fAnchor;
  std::cout << "\n\t" << std::flush;
    
  if (scheme > 0) std::cout << std::setw(scheme) << "scheme" << '|';
  if (user   > 0) {
    std::cout << std::setw(user) << "user";
    if (passwd > 0) 
      std::cout << '|' << std::setw(passwd) << "passwd";
    std::cout << '|';
  }
  if (host   > 0) {
    std::cout << std::setw(host) << "host";
    if (port > 0) 
      std::cout << '|' << std::setw(port) << "port";
  }
  if (path   > 0) std::cout << std::setw(path) << "path";
  if (query  > 0) std::cout << '|' << std::setw(query) << "query";
  if (anchor > 0) std::cout << '|' << std::setw(anchor) << "anchor";
  std::cout << std::endl;
  std::vector<std::string> opts = Options();
  if (opts.size() <= 0) return;
  std::cout << "Options: " << std::flush;
  for (std::vector<std::string>::iterator i = opts.begin(); 
       i != opts.end(); ++i) 
    std::cout << (*i) << " ";
  std::cout << std::endl;
}
//
// EOF
//
