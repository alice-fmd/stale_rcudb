#ifdef HAVE_CONFIG_H
# include "config.h"
#else 
# define VERSION "?.?"
#endif
#include <rcudb/Server.h>
#include <rcudb/Sql.h>
#include <rcudb/Result.h>
#include <rcudb/Row.h>
#include "Options.h"
#include <iostream>
#include <string>
#include <vector>

struct Config 
{
  Config(RcuDb::Row& row) 
  {
    row.Field(0, fId);
    row.Field(1, fTag);
    row.Field(2, fX);
    row.Field(3, fY);
    row.Field(4, fZ);
    row.Field(5, fPriority);
    row.Field(6, fVersion);
    fDescription=std::string(row.FieldStr(7),row.FieldLen(7));
  }
  void Print() const 
  {
    std::cout << "Config: id="     << fId 
	      << "\ttag="          << fTag
	      << "\tx="            << fX
	      << "\ty="            << fY
	      << "\tz="            << fZ
	      << "\tpriorityid="   << fPriority
	      << "\tversion="      << fVersion 
	      << "\tdescription='" << fDescription 
	      << "'"               << std::endl;
  }
  int fId;
  int fTag;
  int fX;
  int fY;
  int fZ;
  int fPriority;
  int fVersion;
  std::string fDescription;
};
    

bool
doQuery(RcuDb::Server& s, const std::string& sql) 
{
  RcuDb::Sql     query  = sql;
  RcuDb::Result* result = s.Query(query);
  if (!result) {
    std::cerr << "Query failed: " << s.ErrorString() 
	      << std::endl;
    return false;
  }
  RcuDb::Row* row = 0;
  while ((row = result->Next())) {
    Config c(*row);
    c.Print();
  }
  result->Print();
  delete result;
  return true;
}

    
int
main(int argc, char** argv) 
{
  using namespace RcuDb;
  Option<bool>        hOpt('h', "help",   "Show this help", false,false);
  Option<bool>        VOpt('V', "version","Show version number", false,false);
  Option<std::string> cOpt('c', "connection", "Database connection url", 
			   "mysql://config:secret@localhost/RCU");
  // "oracle://fmdfero:fmd1234@alidcscom020/alidcsdb"
  // "SELECT * FROM USER_TABLESPACES"
  CommandLine cl("QUERY");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << PACKAGE_STRING << std::endl;
    return 0;
  }
  
  std::string con   = cOpt;
  RcuDb::Server* server = RcuDb::Server::Connect(con);
  if (!server) {
    std::cerr << "Failed to connect to " << con << std::endl;
    return 1;
  }
  if (!server->IsConnected()) {
    std::cerr << "Not connected to " << con << ": " 
	      << server->ErrorString() << std::endl;
    return 1;
  }
  typedef std::vector<std::string> query_list;
  for (query_list::const_iterator i = cl.Remain().begin(); 
       i != cl.Remain().end(); ++i) {
    doQuery(*server, *i);
  }
  if (cl.Remain().size() <= 0) doQuery(*server, "SELECT * FROM Config");
    
  return 0;
}
