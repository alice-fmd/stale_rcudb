// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT DB interface 
//
/** @file    Server.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of abstract DB interface 
*/
#ifndef RCUDB_SERVER
#define RCUDB_SERVER
#ifndef RCUDB_URL_H
# include <rcudb/Url.h>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace RcuDb
{
  // Forward decls 
  class Sql;
  class Result;
  
  /** @class Server Server.h <rcudb/Server.h>
      @ingroup DB
      @brief Database server base class */ 
  class Server 
  {
  public:
    /** Types of columns */
    enum SqlType {
      /** No type */
      kNone = -1, 
      /** Character (array)*/
      kChar = 1, 
      /** Variable character array */ 
      kVarChar, 
      /** Integer */
      kInt, 
      /** Float */
      kFloat, 
      /** Double */
      kDouble, 
      /** Numeric */
      kNumeric, 
      /** Binary BLOB */
      kBinary, 
      /** Time stamp */
      kTimeStamp
    };
    /** Destructor */
    virtual ~Server() {}
      
    /** Close the connection */
    virtual void Close() = 0;
    /** Do a query 
	@param query SQL Query 
	@return A result set of the query.  The user must manage the
	memory of the returned object */ 
    virtual Result* Query(const Sql& query) = 0;
    /** Do a query with no result set returned. 
	@param query SQL Query 
	@return @c true on success, @c false otherwise */
    virtual bool Exec(const Sql& query);
    /** Check if we're connected */ 
    virtual bool IsConnected() const { return fUrl.Port() != -1; }

    /** Start a transaction 
	@return @c true on succcess */ 
    virtual bool StartTransaction();
    /** End the transaction by commiting changes 
	@return @c true on succcess */ 
    virtual bool Commit();
    /** Roll back to last started transaction state 
	@return @c true on succcess */ 
    virtual bool RollBack();
      
    /** Get the type of the database as a string 
	@return Database type as a string */
    const std::string& DBMS() const { return fUrl.Scheme(); }
    /** Get the host as a string 
	@return The host name */ 
    const std::string& Host() const { return fUrl.Host(); }
    /** Get the data base as a string 
	@return Database name */ 
    const std::string& DB() const { return fUrl.Path(); }
    /** Get the port number 
	@return The port number */ 
    int Port() const { return fUrl.Port(); }
	
    /** Check if we have an error */
    virtual bool IsError() const { return ErrorCode() != 0; }
    /** Get the error code 
	@return Last error code */ 
    virtual int ErrorCode() const { return fErrorCode; }
    /** Get the error string 
	@return Last error string */ 
    virtual const std::string& ErrorString() const { return fErrorString; }
    /** Clear the errors */ 
    virtual void ClearError() { fErrorCode = 0; fErrorString = ""; }
      
    /** Access static member function. 
	@code 
	RcuDb::Server* server = \
	RcuDb::Server::Connect("mysql://localhost/FOO");
	@endcode 
	@param url Connection parameters 
	@return Pointer to newly allocated DB connection */
    static Server* Connect(const std::string& url);
  protected:
    /** Constructor is hidden from user */ 
    Server(const Url& u);
    /** Service function to set the error code and string */ 
    void SetError(int code, const std::string& str);
    /** The URL */ 
    Url fUrl;
    /** Error code */ 
    int fErrorCode;
    /** Error string */ 
    std::string fErrorString;
  };
}
#endif
//
// EOF
//

