// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT Mysql interface 
//
/** @file    MysqlRow.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:35:12 2007
    @brief   Declaration of Mysql::Row class 
*/
#ifndef DCACCESS_MYSQL_ROW
#define DCACCESS_MYSQL_ROW
#ifndef RCUDB_ROW
# include <rcudb/Row.h>
#endif
#ifndef __MYSQL_H__
# include <mysql.h>
#endif

namespace RcuDb 
{
  /** @defgroup MySQL MySQL interface classes 
      @ingroup DB 
  */
  /** @namespace Mysql 
      @brief Namespace for Mysql code 
      @ingroup MySQL
  */
  namespace Mysql 
  {
    // forward decls 
    class Result;
    
    //________________________________________________________________
    /** @class Row
	@brief Specialisation of result row for MySQL 
	@ingroup MySQL
     */ 
    class Row : public RcuDb::Row 
    {
    public:
      /** Destructor */
      virtual ~Row() { Close(); }
      /** Close the result */
      virtual void Close();
      /** Get field as a string 
	  @param i field number */
      virtual const char* FieldStr(int i);
      /** Get length of field string 
	  @param i Field number */ 
      virtual unsigned long FieldLen(int i);
    private:
      /** Constructor */ 
      Row(MYSQL_RES* r, MYSQL_ROW f) 
	: fResult(r), fFields(f), fLengths(0) 
      {}
      /** Declare result to be a friend */ 
      friend class Result;
      /** The current result set */ 
      MYSQL_RES* fResult;
      /** The fields */ 
      MYSQL_ROW  fFields;
      /** The lengths of each field */
      unsigned long* fLengths;
      /** Check if this is valid */
      bool Validate(int i);
    };
  }
}
#endif
//
// EOF
//

