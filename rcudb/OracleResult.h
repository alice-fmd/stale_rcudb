// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT Oracle interface 
//
/** @file    OracleResult.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of Oracle::Result class
*/
#ifndef RCUDB_ORACLE_RESULT
#define RCUDB_ORACLE_RESULT
#ifndef RCUDB_RESULT
# include <rcudb/Result.h>
#endif
#ifndef __OCCI_H__
# include <occi.h>
#endif
#ifndef __DEQUE__
# include <deque>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace RcuDb 
{
  // Forward decls 
  class Row;

  namespace Oracle 
  {
    // forward decls 
    class Server;
    class Row;

    /** @class Result
        @brief Specialisation of result result for Oracle 
	@ingroup Oracle
    */ 
    class Result : public RcuDb::Result 
    {
    public:
      /** Destructor */
      virtual ~Result() { Close(); }
      /** Close the result */
      virtual void Close();
      /** Get the number of fields in this result set */ 
      virtual int NFields();
      /** Get the name of the field @a i */
      virtual const char* FieldName(int i);
      /** Get the next row in the result set.  This can be used to
	  iterate over the result set. 
	  @return next row, or 0 if there's no more rows left */
      virtual RcuDb::Row* Next();
    private:
      /** Constructor */ 
      Result(oracle::occi::Connection* c, oracle::occi::Statement* s);
      /** Check if this is valid */
      bool Validate(int i);
      /** Declare server to be a friend */ 
      friend class Server;
      /** Connection */
      oracle::occi::Connection* fCon;
      /** Statement */
      oracle::occi::Statement* fStatement;
      /** Result */
      oracle::occi::ResultSet* fResult;
      /** Meta data */ 
      typedef std::vector<oracle::occi::MetaData> MetaVector;
      /** Meta data */ 
      MetaVector* fMeta;
      /** The field count */ 
      int fNFields;
      /** pool of results */ 
      typedef std::deque<Oracle::Row*> Pool_t;
      /** pool of results */ 
      Pool_t fPool;
      /** Name buffer */ 
      std::string fNameBuffer;
    };
  }
}
#endif
//
// EOF
// 
