//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT MySQL interface 
//
/** @file    Server.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:03:14 2007
    @brief   Implementation of abstract Database access 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "Server.h"
#include "Result.h"
#include "Sql.h"
#ifdef HAVE_MYSQL
# include "MysqlServer.h"
#endif
#ifdef HAVE_ORACLE
# include "OracleServer.h"
#endif
#include <iostream>
#include <iomanip>


//____________________________________________________________________
RcuDb::Server*
RcuDb::Server::Connect(const std::string& url) 
{
  Url u(url);
  
  RcuDb::Server* server = 0;
#ifdef HAVE_MYSQL
  if (u.Scheme() == "mysql")
    server = new Mysql::Server(u);
#endif
#ifdef HAVE_ORACLE
  if (u.Scheme() == "oracle") 
    server = new Oracle::Server(u);
#endif
  if (!server) {
    std::cerr << "Unsupported DB Url: \"" << url << "\"\n"
	      << "Possible options are\n" << std::endl;
#ifdef HAVE_MYSQL
    std::cerr << "MySQL database:\n"
	      << "\tmysql://[<user>[:<password>]@][<host>][:<port>]/[scheme]\n"
	      << std::endl;
#endif
#ifdef HAVE_ORACLE
    std::cerr << "Oracle database:\n"
	      << "\toracle://<user>[:<password>]@<host>[:<port>]/[service]\n"
	      << "\toracle://<user>[:<password>]@///<host>[:port]/[service]\n"
	      << "\toracle://<user>[:<password>]@/[tns alias]\n"
	      << std::endl;
#endif
    std::cerr << "where <host> is a remote host, <port> is the port the\n"
	      << "database server is listning on, <user> is the login\n"
	      << "name, <password> is the password, and <database> is a\n"
	      << "database on the server." << std::endl;
  }
  return server;
}

//____________________________________________________________________
RcuDb::Server::Server(const Url& u)
  : fUrl(u), 
    fErrorCode(0)
{}

//____________________________________________________________________
bool
RcuDb::Server::Exec(const Sql& query) 
{
  Result* res = Query(query);
  if (!res) return false;
  delete res;
  return true;
}

//____________________________________________________________________
void
RcuDb::Server::SetError(int code, const std::string& str) 
{
  fErrorCode    = code;
  fErrorString  = str;
}

//____________________________________________________________________
bool
RcuDb::Server::StartTransaction() 
{
  return Exec("START TRANSACTION");
}

//____________________________________________________________________
bool
RcuDb::Server::Commit() 
{
  return Exec("COMMIT");
}

//____________________________________________________________________
bool
RcuDb::Server::RollBack() 
{
  return Exec("ROLLBACK");
}


//
// EOF
//
