#include "Blob.h"
#include <iostream>
#include <vector>

int
main()
{
  RcuDb::Blob blob;
  
  std::vector<int> v;
  for (size_t i = 0; i < 5; i++) v.push_back(i);
  
  blob.Encode(v);
  std::cout << blob << std::endl;
  
  std::vector<int> u;
  blob.Decode(u);
  std::cout << "Decoded: " << std::endl;
  for (size_t i = 0; i < u.size(); i++) 
    std::cout << v[i] << " <> " << u[i] << std::endl;
  
  return 0;
}
