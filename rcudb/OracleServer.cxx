//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT Oracle interface 
//
/** @file    OracleServer.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:03:14 2007
    @brief   Implementation of Oracle::Server class
*/
#include "OracleServer.h"
#include "OracleResult.h"
#include "Sql.h"
#include <iostream>
#ifndef ORACLE_PORT
/** Default Port for Oracle DBMS */
# define ORACLE_PORT 1521
#endif

//====================================================================
RcuDb::Oracle::Server::Server(const Url& url) 
  : RcuDb::Server(url), 
    fEnv(0),
    fCon(0)
{
  if (fUrl.Port() <= 0) fUrl.SetPort(ORACLE_PORT);

  std::string db   = fUrl.Path();
  std::string host = fUrl.Host();
  std::string con  = fUrl.Path();
  if (db[0] == '/') db.erase(0,1);
  if (!host.empty()) {
    std::stringstream scon;
    scon << "//" << fUrl.Host() << ":" << fUrl.Port() << "/" << db;
    con = scon.str();
    fUrl.SetPath(db);
  }

  try {
    fEnv  = oracle::occi::Environment::createEnvironment();
    fCon  = fEnv->createConnection(fUrl.User(), fUrl.Password(), con);
  }
  catch (oracle::occi::SQLException& e) {
    SetError(e.getErrorCode(), e.getMessage());
    fUrl.SetPort(-1);
  }
}

//____________________________________________________________________
bool
RcuDb::Oracle::Server::CheckConnection() 
{
  ClearError();
  if (IsConnected()) return true;
  
  SetError(-1, "Not connected to Oracle server");
  return false;
}

//____________________________________________________________________
RcuDb::Oracle::Server::~Server()
{
  if (!IsConnected()) return;
  Close();
  delete fCon;
}


//____________________________________________________________________
void
RcuDb::Oracle::Server::Close()
{
  ClearError();
  try {
    if (fCon && fEnv) fEnv->terminateConnection(fCon);
    if (fEnv)         oracle::occi::Environment::terminateEnvironment(fEnv);
  }
  catch (oracle::occi::SQLException& e) {
    SetError(e.getErrorCode(), e.getMessage());
  }
}


//____________________________________________________________________
RcuDb::Result* 
RcuDb::Oracle::Server::Query(const Sql& query) 
{
  if (!CheckConnection()) return 0;

  Result* res = 0;
  try {
    oracle::occi::Statement* stmt = fCon->createStatement();
    stmt->setSQL(query.Text());
    stmt->setPrefetchRowCount(1000);
    stmt->setPrefetchMemorySize(10000000);
    stmt->execute();
    res = new Result(fCon, stmt);
  }
  catch (oracle::occi::SQLException& e) {
    SetError(e.getErrorCode(), e.getMessage());
  }
  return res;
}


//____________________________________________________________________
bool
RcuDb::Oracle::Server::Exec(const Sql& query) 
{
  if (!CheckConnection()) return false;

  bool res = false;
  oracle::occi::Statement* stmt = 0;
  try {
    stmt = fCon->createStatement(query.Text());
    stmt->execute();
    res = true;
  }
  catch (oracle::occi::SQLException& e) {
    SetError(e.getErrorCode(), e.getMessage());
    res = false;
  }

  try {
    fCon->terminateStatement(stmt);
  }
  catch (oracle::occi::SQLException& e) {
    SetError(e.getErrorCode(), e.getMessage());
    res = false;
  }
  return res;
    
}

//____________________________________________________________________
bool 
RcuDb::Oracle::Server::StartTransaction() 
{
  return Commit();
}


//____________________________________________________________________
bool 
RcuDb::Oracle::Server::Commit()
{
  if (!CheckConnection()) return false;

  try {
    fCon->commit();
  }
  catch (oracle::occi::SQLException& e) {
    SetError(e.getErrorCode(), e.getMessage());
    return false;
  }
  return true;
}


//____________________________________________________________________
bool 
RcuDb::Oracle::Server::RollBack()
{
  if (!CheckConnection()) return false;

  try {
    fCon->rollback();
  }
  catch (oracle::occi::SQLException& e) {
    SetError(e.getErrorCode(), e.getMessage());
    return false;
  }
  return true;
}

//
// EOF
//
