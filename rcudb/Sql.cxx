//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT MySQL interface 
//
/** @file    Sql.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:16:56 2007
    @brief   Definition of Sql class
*/
#include "Sql.h"
#include <cstdarg>
#include <iostream>

  
//____________________________________________________________________
RcuDb::Sql::Sql(const std::string& s) 
  : fStr(s)
{}

//____________________________________________________________________
RcuDb::Sql::Sql(const char* format, ...) 
{
  va_list ap;
  va_start(ap, format);
  static char buf[2048];
  vsnprintf(buf, 2048, format, ap);
  va_end(ap);
  fStr = buf;
}

//____________________________________________________________________
RcuDb::Sql&
RcuDb::Sql::operator=(const Sql& q) 
{
  fStr = q.fStr;
  return *this;
}

//________________________________________________________________
std::ostream& 
RcuDb::operator<<(std::ostream& o, const RcuDb::Sql& q) 
{
  o << q.fStr;
  return o;
}

//
// EOF
//
