//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT MySQL interface 
//
/** @file    MysqlRow.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:43:04 2007
    @brief   Definition of Mysql::Row class 
*/
#include "MysqlRow.h"

//____________________________________________________________________
void
RcuDb::Mysql::Row::Close() 
{
  if (!fFields) return;
  fFields  = 0;
  fResult  = 0;
  fLengths = 0;
}

//____________________________________________________________________
const char* 
RcuDb::Mysql::Row::FieldStr(int i) 
{
  if (!Validate(i)) return 0;
  return fFields[i];
}

//____________________________________________________________________
unsigned long
RcuDb::Mysql::Row::FieldLen(int i) 
{
  if (!Validate(i)) return 0;
  if (!fLengths) fLengths = mysql_fetch_lengths(fResult);
  if (!fLengths) return 0; // Bad! 
  return fLengths[i];
}

//____________________________________________________________________
bool
RcuDb::Mysql::Row::Validate(int i)
{
  if (!fFields) return false; // Closed
  if (i < 0 || i >= int(mysql_num_fields(fResult))) return false;
  return true;
}

//
// EOF
//
