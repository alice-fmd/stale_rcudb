// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT DB interface 
//
#ifndef RCUDB_BLOB_H
#define RCUDB_BLOB_H
#ifndef RCUDB_BYTESWAP
# include <rcudb/ByteSwap.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

/** @defgroup DB Database classes 
    @brief Database classes */

/** @namespace DB 
    @brief Namespace for database classes 
    @ingroup DB
*/
namespace RcuDb
{
  //================================================================
  /** @class Blob 
      @brief A Binary Large OBject
      @ingroup DB
      This class can be used to store binary large objects in a
      database table, in an architecture, database manager
      independent way.  The binary data is encoded as a hexadecimal
      0 terminated string.  
  */
  class Blob 
  {
  public:
    /** Constructor 
	@param data Data to put into the blob
	@param size Number of elements in data. */
    template <typename T> 
    Blob(const T* data, size_t size) { Encode(data, size); }
    /** Constructor 
	@param data Data to put into the blob. */
    template <typename T>
    Blob(const std::vector<T>& data) { Encode(data); }
    /** Constructor 
	@param data Data to put into the blob. 
	@param size Number of bytes in @a data */
    explicit Blob(unsigned char* data, size_t size) {Encode(data,size); }
    /** Constructor */
    Blob() : fData("") {}

    /** Encode the array @a data of size @a size of type @a T into a
	null terminated string.  The data is converted to big endian
	first (if needed) an then each bytes is represented by two
	hexadecimal characters. 
	@param data The data to encode. 
	@param size Number of elements of type @a T in @a data */
    template <typename T>
    void Encode(const T* data, size_t size);
    /** Encode the array @a data of type @a T into a null terminated
	string.  The data is converted to big endian first (if
	needed) an then each bytes is represented by two hexadecimal
	characters. 
	@param data The data to encode. */
    template <typename T>
    void Encode(const std::vector<T>& data) {Encode(&(data[0]),data.size());}
    /** Encode blob into a null terminated string.  Each byte is
	encoded as two hexadecimal characters. 
	@param data The bytes to write 
	@param size The number of bytes. */
    void Encode(unsigned char* data, size_t size);

    /** Decode the encoded string into a byte array.  The bytes are
	returned in the newly created vector @a data 
	@param data On return, it holds the bytes */
    void Decode(std::vector<unsigned char>& data) const;
    /** Decode the encoded string into an array of type @a T.
	The elements are returned in the newly created vector @a
	data.  If needed, the bytes of the elements are swapped
	before returning. 
	@param data On return, it holds the elements. */
    template <typename T>
    void Decode(std::vector<T>& data) const;

    /** Get the string representation of the blob. 
	@return The null terminated string representation of the
	blob */
    const std::string& String() const { return fData; }
      
    /** Assign from an already encoded string 
	@param s Encoded string 
	@return Reference to this object */ 
    Blob& operator=(const std::string& s) { fData = s; return *this; }
  protected:
    /** The data as a zero terminated string */ 
    std::string fData;
  };
    
  //________________________________________________________________
  template <typename T>
  inline void
  Blob::Encode(const T* data, size_t size) 
  {
    std::vector<T> t(size);
    for (size_t j = 0; j < size; j++) t[j] = Host2Net(data[j]);
    size_t n = sizeof(T) * size;
    T* pt = &(t[0]);
    unsigned char* source = reinterpret_cast<unsigned char*>(pt);
    Encode(source, n);
  }
  //________________________________________________________________
  template <typename T>
  inline void
  Blob::Decode(std::vector<T>& data) const
  {
    std::vector<unsigned char> bytes;
    Decode(bytes);
    data.resize(bytes.size() / sizeof(T));
    T* source = reinterpret_cast<T*>(&(bytes[0]));
    for (size_t j = 0; j < data.size(); j++) data[j] = Host2Net(source[j]);
  }
  //________________________________________________________________
  /** Output operator of a blob 
      @param o Output stream 
      @param b Blob object */
  extern std::ostream& operator<<(std::ostream& o, const Blob& b);
  //________________________________________________________________
  /** Input operator of a blob 
      @param i input stream 
      @param b Blob object */
  extern std::istream& operator>>(std::istream& i, Blob& b);
}

#endif
//
// EOF
//
