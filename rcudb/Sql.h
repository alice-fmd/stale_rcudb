// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT DB interface 
//
/** @file    Sql.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:13:24 2007
    @brief   Declaration of Sql class 
*/
#ifndef RCUDB_SQL
#define RCUDB_SQL
#ifndef __STRING__
# include <string>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

namespace RcuDb 
{
  //================================================================
  /** @class Sql
      @ingroup DB
      @brief Class representing SQL statements */
  class Sql 
  {
  public:
    /** Constructor 
	@param s Sql text */
    Sql(const std::string& s=std::string());
    /** Constructor 
	@param format @c printf like format statement. */
    Sql(const char* format, ...);
    /** Get the text of this query 
	@return A string containing the text of this query */
    const std::string& Text() const { return fStr; }
    /** Assignment operator 
	@param q Other query object 
	@return Reference to this object */
    Sql& operator=(const Sql& q);
  protected:
    /** Internal string stream. */
    std::string fStr;
    // Put-to operator is a friend 
    template <typename T>
    friend Sql& operator<<(Sql& q, const T& x);
    // Stream put-to operator is a friend 
    friend std::ostream& operator<<(std::ostream& o, const Sql& q);
  };
    
  //________________________________________________________________
  /** Put-to operator for Sql statements.  The argument @a x is
      written to the Sql statement using the put-to operator for the
      type @a T.  
      @param q Sql statement 
      @param x Something to write to the Sql statement. 
      @return @a q after appending @a x to it. */
  template <typename T>
  inline 
  Sql& operator<<(Sql& q, const T& x) 
  {
    std::stringstream s;
    s << q.fStr << x;
    q.fStr = s.str();
    return q;
  }
  //________________________________________________________________
  /** Put-to operator of Sql statements for standard output
      streams. 
      @param o Output stream
      @param q Sql statement. 
      @return @a o after appending @a q to it. */
  extern std::ostream& operator<<(std::ostream& o, const Sql& q);
}
#endif
//
// EOF
//

