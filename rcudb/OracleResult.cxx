//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT Oracle interface 
//
/** @file    OracleResult.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:03:14 2007
    @brief   Implementation of Oracle::Result class
*/
#include "OracleResult.h"
#include "OracleRow.h"
#include <iostream>

//====================================================================
RcuDb::Oracle::Result::Result(oracle::occi::Connection* c, 
			      oracle::occi::Statement* s)
  : fCon(c), 
    fStatement(0),
    fResult(0),
    fMeta(0),
    fNFields(0)
{
  if (!s) {
    std::cerr << "Empty statement" << std::endl;
    return;
  }

  // Some of these calls may cause exceptions.  That's OK, we catch
  // them in the server. 
  // Basic information
  fStatement  = s;
  if (s->status()  != oracle::occi::Statement::RESULT_SET_AVAILABLE) return;
  fResult     = s->getResultSet();
  if (!fResult) return;
  fMeta       = new MetaVector(fResult->getColumnListMetaData());
  fNFields    = (fMeta  ? fMeta->size() : 0);
  
  // Pool rows.  These are put in a FIFO 
  if (!fResult) return;
  while (fResult->next()) {
    fNRows++;
    fPool.push_back(new Row(fResult, fMeta));
  }
}

//____________________________________________________________________
void
RcuDb::Oracle::Result::Close()
{
  if (fCon && fStatement) {
    if (fResult) fStatement->closeResultSet(fResult);
    fCon->terminateStatement(fStatement);
  }
  
  for (Pool_t::iterator i = fPool.begin(); i != fPool.end(); ++i) 
    delete (*i);
  fPool.clear();
  
  if (fMeta) fMeta->clear();

  fStatement = 0;
  fResult    = 0;
  fMeta      = 0;
} 

//____________________________________________________________________
const char*
RcuDb::Oracle::Result::FieldName(int i) 
{
  if (!Validate(i) || !fMeta) return 0;
  fNameBuffer = (*fMeta)[i].getString(oracle::occi::MetaData::ATTR_NAME);
  return fNameBuffer.c_str();
}

//____________________________________________________________________
int 
RcuDb::Oracle::Result::NFields() 
{
  return fNFields;
}

//____________________________________________________________________
RcuDb::Row*
RcuDb::Oracle::Result::Next() 
{
  if (!fResult || fPool.size() == 0) return 0;
  Row* r = fPool.front();
  fPool.pop_front();
  return r;
}


//____________________________________________________________________
bool
RcuDb::Oracle::Result::Validate(int i) 
{
  if (!fResult) return false; // Closed
  if (i < 0 || i >= NFields()) return false;
  return true;
}  

//
// EOF
//
