//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT MySQL interface 
//
/** @file    Result.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:29:38 2007
    @brief   Definition of result class 
*/
#include "Result.h"
#include "Row.h"
#include <vector>
#include <iomanip>
#include <iostream>

//____________________________________________________________________
void
RcuDb::Result::Print()
{
  std::vector<unsigned long> sizes(NFields());
  std::vector<Row*>          rows;
  Row* row  = 0;
  while ((row = Next())) {
    rows.push_back(row);
    for (int i = 0; i < NFields(); i++) 
      sizes[i] = std::max(sizes[i], row->FieldLen(i));
  }
  
  std::cout << "Result of Query: " << NRows() << " rows" << std::endl;
  for (int i = 0; i < NFields(); i++) {
    unsigned long l = strlen(FieldName(i));
    sizes[i] = std::max(sizes[i], l);    
    std::cout << " " << std::setw(sizes[i]) << FieldName(i) << " |";
  }
  std::cout << "\n" << std::setfill('-') << std::flush;
  for (int i = 0; i < NFields(); i++) 
    std::cout << std::setw(sizes[i]+2) << '-' << '+';
  std::cout << "\n" << std::setfill(' ') << std::left << std::flush;
  for (std::vector<Row*>::iterator r = rows.begin(); r != rows.end(); ++r) {
    for (int i = 0; i < NFields(); i++) 
      std::cout << " " << std::setw(sizes[i]) 
		<< ((*r)->FieldLen(i) > 0 && (*r)->FieldStr(i) 
		    ? (*r)->FieldStr(i) : "") << " |";
    std::cout << std::endl;
  }
}

//____________________________________________________________________
//
// EOF
//
