#include <iostream>
#include <iomanip>
#include "Url.h"


int
main(int argc, char** argv) 
{
  RcuDb::Url url("foo://example.com:8042/over/there?name=ferret#nose");
  url.Print();
  url     = "urn:example:animal:ferret:nose";
  url.Print();
  url     = "http://localhost/foo/bar/baz";
  url.Print();
  url     = "http://localhost/foo/bar/baz/";
  url.Print();
  url     = "http://localhost/";
  url.Print();
  url     = "http://user:pass@localhost/";
  url.Print();
  url     = "http://user:pass@localhost/foo/bar";
  url.Print();
  url     = "http://user:pass@localhost/foo/bar/";
  url.Print();
  url     = "http://user:pass@localhost/foo/bar/?query=bla";
  url.Print();
  url     = "http://user:pass@localhost/foo/bar/?query=bla#anchor";
  url.Print();
  url     = "http://user:pass@localhost/foo/bar/?a=bar&b=foo&c=hello";
  url.Print();
  

  for (int i = 1; i < argc; i++) {
    url = argv[i];
    url.Print();
  }
  
  while (true) {
    std::cout << "Please input a URL: " << std::flush;
    std::string u;
    std::getline(std::cin, u);
    if (u == "q") break;
    url = u;
    url.Print();
  }
  return 0;
}

    
