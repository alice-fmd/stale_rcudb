//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
// Note, some of this code is based on the ROOT MySQL interface 
//
/** @file    MysqlResult.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Mar 29 01:43:04 2007
    @brief   Definition of Mysql::Row class 
*/
#include "MysqlResult.h"
#include "MysqlRow.h"

//____________________________________________________________________
RcuDb::Mysql::Result::Result(MYSQL_RES* res)
{
  fResult = res;
  fNRows  = res ? mysql_num_rows(fResult) : 0;
  fInfo   = 0;
}

//____________________________________________________________________
void
RcuDb::Mysql::Result::Close()
{
  if (!fResult) return;
  mysql_free_result(fResult);
  fResult = 0;
  fInfo   = 0;
  fNRows  = 0;
} 

//____________________________________________________________________
const char*
RcuDb::Mysql::Result::FieldName(int i) 
{
  if (!Validate(i)) return 0;
  if (!fInfo) fInfo = mysql_fetch_fields(fResult);
  if (!fInfo) return 0; // Bad 
  return fInfo[i].name;
}

//____________________________________________________________________
int 
RcuDb::Mysql::Result::NFields() 
{
  if (!fResult) return 0;
  return int(mysql_num_fields(fResult));
}

//____________________________________________________________________
RcuDb::Row*
RcuDb::Mysql::Result::Next() 
{
  if (!fResult) return 0;
  MYSQL_ROW row = mysql_fetch_row(fResult);
  if (!row) return 0;
  return new Row(fResult, row);
}


//____________________________________________________________________
bool
RcuDb::Mysql::Result::Validate(int i) 
{
  if (!fResult) return false; // Closed
  if (i < 0 || i >= NFields()) return false;
  return true;
}  
//
// EOF
//
