#
#  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation; either version 2.1
#  of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
#  02111-1307 USA
#
#
#
AUTOMAKE_OPTIONS	= foreign
ACLOCAL_AMFLAGS		= -I .
CLEANFILES		= *.root *.dat build-stamp
SUBDIRS			= $(PACKAGE) doc support debian
EXTRA_DIST		= config/depcomp

origtar		       = $(distdir:@PACKAGE@-%=@PACKAGE@_%).orig.tar.gz

orig:	distdir
	mv $(distdir) $(distdir).orig
	tardir=$(distdir).orig && $(am__tar) | \
		GZIP=$(GZIP_ENV) gzip -c >$(origtar)
	rm -rf $(distdir).orig

rpm: dist
	sudo rpmbuild -ta $(distdir).tar.gz

deb: orig
	$(MAKE) -C debian control
	mv $(origtar) ../
	DEB_BUILD_OPTIONS="nostrip noopt" debuild -sa -ICVS -i

release:
	@if test "x$(MSG)" = "x" ; then \
	  echo "No MSG specified, please run 'make $@ MSG=\"...\"'" ; false ;\
	fi
	@dch -i "$(MSG)" 
	@r=`head -n1 debian/changelog |sed 's/$(PACKAGE) ([0-9][0-9.]*-\([0-9][0-9]*\)).*/\1/'` ; \
	  sed "s/\(Release:[[:space:]]*\)\([0-9][0-9]*\)/\1$${r}/" \
		< support/$(PACKAGE).spec.in > support/$(PACKAGE).spec.tmp ; \
	  echo "New release is $(VERSION)-$$r"
	@csplit -s -f support/$(PACKAGE).spec. support/$(PACKAGE).spec.tmp \
		'/%changelog/+1' 
	@d=`date "+%a %b %d %Y"` ; \
	  m=`perl -e 'my @pw=getpwuid $$<;$$pw[6]=~s/,.*//;print $$pw[6]'`; \
	  e=`perl -e 'my $$addr; open MAILNAME, "/etc/mailname" ; chomp($$addr=<MAILNAME>); close MAILNAME; my $$user=getpwuid $$<; $$addr="$$user\@$$addr"; print $$addr;'` ; \
	  echo "* $$d $$m <$$e>" >> support/$(PACKAGE).spec.00 ; \
	  echo "- $(MSG)"        >> support/$(PACKAGE).spec.00 ; \
	  echo ""                >> support/$(PACKAGE).spec.00 ; 
	cat support/$(PACKAGE).spec.0? > support/$(PACKAGE).spec.in
	rm -f support/$(PACKAGE).spec.0? support/$(PACKAGE).spec.tmp

#
# EOF
#
