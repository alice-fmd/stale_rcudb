dnl
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_PROFILING],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([profiling],
	        [AC_HELP_STRING([--enable-profiling],
			        [Compile code to enable profiling])],
                [],[enable_profiling=no])
  AC_MSG_CHECKING([whether to enable profiling])
  if test "x$enable_profiling" = "xyes" ; then 
    CFLAGS="$CFLAGS -pg" 
    CXXFLAGS="$CXXFLAGS -pg"
    LDFLAGS="$LDFLAGS -pg"
  fi
  AC_MSG_RESULT([$enable_profiling])
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_STRICT],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([strict],
	        [AC_HELP_STRING([--enable-strict],
			        [Require strictly correct code])],
                [],[enable_strict=no])
  AC_MSG_CHECKING([whether require strictly correct code])
  if test "x$enable_strict" = "xyes" ; then 
    CFLAGS="$CFLAGS -Wall -Werror -pedantic -ansi" 
    # Cannot use `-pedantic' due to use of `long long'
    CXXFLAGS="$CXXFLAGS -Wall -Werror -ansi"
  fi
  AC_MSG_RESULT([$enable_strict ($CFLAGS)])
])
  
dnl ------------------------------------------------------------------
dnl
dnl Check for Oracle
dnl AC_ORACLE([ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
dnl
AC_DEFUN([AC_ORACLE],
[
  AC_ARG_WITH([oracle-libdir],
	      [AC_HELP_STRING([--with-oracle-libdir=DIR],
	        [Specify Oracle client library installation path])])
  AC_ARG_WITH([oracle-incdir],
	      [AC_HELP_STRING([--with-oracle-incdir=DIR],
		[Specify Oracle client header installation path])])

  AC_LANG_PUSH([C++])
  save_LDFLAGS=$LDFLAGS
  save_CPPFLAGS=$CPPFLAGS
  if test ! "x$with_oracle_libdir" = "x" && \
     test ! "x$with_oracle_libdir" = "xyes" ; then 
    ORACLE_LDFLAGS="-L$with_oracle_libdir -Wl,-rpath,$with_oracle_libdir"
  fi
  if test ! "x$with_oracle_incdir" = "x" && \
     test ! "x$with_oracle_incdix" = "xyes" ; then 
    ORACLE_CPPFLAGS="-I$with_oracle_incdir"
  fi
  LDFLAGS="$LDFLAGS $ORACLE_LDFLAGS"
  CPPFLAGS="$CPPFLAGS $ORACLE_CPPFLAGS"
  have_oracle=yes
  save_LIBS="$LIBS"
  dnl AC_CHECK_LIB([nnz10],[main],[],[have_oracle=no], [-lssl])
  AC_CHECK_LIB([clntsh],[main],[],[have_oracle=no], [-lnnz10])
  AC_CHECK_LIB([ociei],[main],[],[have_oracle=no], [-lclntsh -lnnz10])
  AC_CHECK_LIB([occi],[main],[],[have_oracle=no], [-lclntsh -lnnz10 -lociei])
  AC_CHECK_HEADER([occi.h],[],[have_oracle=no])
  AC_LANG_POP([C++])
  LDFLAGS="$save_LDFLAGS"
  CPPFLAGS="$save_CPPFLAGS"
  if test "x$have_oracle" = "xyes" ;  then 
    ORACLE_LIBS="-locci -lclntsh -lociei -lclntsh -lnnz10"
  else
    ORACLE_LDFLAGS=""
    ORACLE_CPPFLAGS=""
  fi
  LIBS="$save_LIBS"
  if test "x$have_oracle" = "xyes" ; then 
    ifelse([$1], , :, [$1])
  else 
    ifelse([$2], , :, [$2])
  fi
  AC_SUBST(ORACLE_LDFLAGS)
  AC_SUBST(ORACLE_CPPFLAGS)
  AC_SUBST(ORACLE_LIBS)
])

dnl ------------------------------------------------------------------
dnl
dnl Check for Mysql
dnl AC_MYSQL([MIN_VERSION=5.0.0
dnl            [, ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]]])
dnl
AC_DEFUN([AC_MYSQL],
[
  AC_ARG_ENABLE([mysql],
	        [AC_HELP_STRING([--enable-mysql],
	                        [Enable MySQL support])], 
	 	[],[enable_mysql=yes])
  AC_ARG_WITH([mysql-prefix],
	      [AC_HELP_STRING([--with-mysql-prefix=DIR],
		[Specify Mysql client installation prefix])], 
	      [with_mysql_prefix=$withval],[with_mysql_prefix=none])

  if test "x$enable_mysql" = "xno" ; then
    have_mysql=no
  else
    if test ! x"$with_mysql_prefix" = xnone; then
      mysql_bin="$with_mysql_prefix/bin"
    else 
      mysql_bin=$PATH
    fi
    AC_PATH_PROG(MYSQL_CONF, mysql_config, no, $mysql_bin)
  	
    have_mysql=no
    if test ! x"$MYSQL_CONF" = "xno" ; then 
      # define some variables 
      changequote(<<, >>)dnl
      MYSQL_CFLAGS=`$MYSQL_CONF --cflags | sed 's/-[ID][^ ]*//g'` 
      MYSQL_CPPFLAGS=`$MYSQL_CONF --cflags | sed 's/\(-[ID][^ ]*\)/\1/g'` 
      MYSQL_LDFLAGS=`$MYSQL_CONF --libs | sed 's/-l[^ ]*//g'`
      MYSQL_LIBS=`$MYSQL_CONF --libs | sed 's/\(-l[^ ]*\)/\1/g'`
      MYSQL_VERS=`$MYSQL_CONF --version` 
      MYSQL_PORT=`$MYSQL_CONF --port` 
      changequote([, ])dnl
  
      # Check the version number is OK.
      mysql_min_version=ifelse([$1], ,5.0.0,$1)
      AC_MSG_CHECKING(if MySQL client library version is >= $mysql_min_version)
      mysql_vers=`echo $MYSQL_VERS | \
        awk 'BEGIN { FS = "."; } \
      	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      mysql_regu=`echo $mysql_min_version | \
        awk 'BEGIN { FS = "."; } \
      	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      if test $mysql_vers -ge $mysql_regu ; then 
           have_mysql=yes
      fi
      AC_MSG_RESULT([$have_mysql - $MYSQL_VERS])
    fi
  fi
  if test "x$have_mysql" = "xyes" ; then 
    ifelse([$2], , :, [$2])
  else 
    ifelse([$3], , :, [$3])
  fi
  AC_SUBST(MYSQL_CFLAGS)
  AC_SUBST(MYSQL_LDFLAGS)
  AC_SUBST(MYSQL_CPPFLAGS)
  AC_SUBST(MYSQL_LIBS)
  AC_SUBST(MYSQL_VERS)
  AC_SUBST(MYSQL_PORT)
])		

#
# EOF
#
