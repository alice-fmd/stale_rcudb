/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage Database Access

    @section intro Database interface 
    
    The database interface is based on the 
    <a href="http://root.cern.ch"><b>ROOT</b></a> abstract SQL database
    interface 
    <a href="http://root.cern.ch/root/html/TSQLServer"><tt>TSQLServer</tt></a>
    The code in this library (see @ref DB) is a reimplementation of
    that interface, and does not require @b ROOT. 

    Currently supported backends are 
    
    - <a href="http://www.mysql.com">MySQL</a> - requires the MySQL C
      client API. 
    - <a href="http://www.oracle.com">Oracle</a> - requires the Oracle
      C++ client API. 

    An instance of a database connection is made using the static
    member funtion RcuDb::Server::Connect, passing a URL as
    argument.  The URL is a standard conforming URL 

    @verbatim
    http://user:pass@example.com:992/animal/bird?species=seagull#wings
    \___/  \________/\_________/\__/\__________/\______________/\____/
      |        |          |       |       |             |          |
    scheme   login      hosts   port    path          query  anchor/fragment
    @endverbatim 

    The @a schema selects the database management type (e.g., @c mysql or
    @c oracle). The @a user part selects the database user name to
    use, @a pass is the (optional) password for @a user. The @a host
    part selects the server host, and the port number is specified in
    @a port.  The @a path part selects the database, and the @a query
    part specifies options for the connection (database managment
    system dependent).  The @a anchor part is generally ignored. 

    To make a connection, do for example 
    @code 
    RcuDb::Url url("mysql://config@localhost/Rcu");
    RcuDb::Server* server = RcuDb::Server::Connect(url);
    if (!server) {
      std::cerr << "Failed to connect to server " << url << std::endl;
      return 1;
    }
    
    @endcode 
    
    Next, we can do queries on the data base tables 
    @code 
    RcuDb::Sql query;
    query << "SELECT * FROM Config WHERE id=" << id;
    RcuDb::Result* result = server->Query(query);
    if (!result) {
      std::cerr << "Query failed: " << server->ErrorString() 
                << std::Endl;
      return 1;
    }
    @endcode 

    The class RcuConf::Sql is a used for making queries.  It can be
    used as a @c std::ostream, meaning we can use the put-to operator
    to make the queries.

    We should always chech the result we get back.  If it is valid
    (not null), it does @e not nessecarily that we got rows back.  We
    should use the RcuDb::Result::Next member function to get
    the results. 
    @code 
    RcuDb::Row* row = 0;
    while ((row = result->Next())) {
    @endcode 

    When we have a row we can extract data from it.  The member
    function template RcuDb::Row::Field can return values of any
    type for which a standard get-from operator is defined 
    @code
      int id;
      row->Field(0, id);
      std::string desc;
      row->Field(1, desc);
      // Process the row data 
    }
    @endcode 

    Once we're done with the result, we should delete it 
    @code 
    delete result;
    @endcode 
    
*/

#error Not for compilation
//
// EOF
//
